package az.ingress.service;

import az.ingress.domain.Hello;
import az.ingress.repository.HelloRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HelloServiceImpl implements HelloService {

    private final HelloRepository helloRepository;

    @Override
    public String sayHello() {
        Hello count = helloRepository.findById(1)
                .orElse(helloRepository.save(new Hello(1, 0L)));
        count.setCount(count.getCount() + 1);
        helloRepository.save(count);
        return "Hello from ms2 " + count.getCount();
    }

}
